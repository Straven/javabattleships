package com.straven.javabattleship;

import java.awt.Graphics;
import java.util.ArrayList;

public class Ship {
    private final ArrayList<Cell> cells = new ArrayList<>();

    Ship(int x, int y, int length, int position) {
        for (int i = 0; i < length; i++)
            cells.add(
                    new Cell(x + i * ((position == 1)? 0 : 1),
                            y + i * ((position == 1)?1:0)));
    }

    // is ship outside the boundary of the field?
    public boolean isOutOfField(int top) {
        for (Cell cell : cells)
            if (cell.getX() < 0 || cell.getX() > top ||
                    cell.getY() < 0 || cell.getY() > top)
                return true;
        return false;
    }

    public boolean isOverlayOrTouch(Ship ctrlShip) { // is ship overlay or touch other ships
        for (Cell cell : cells)
            if (ctrlShip.isOverlayOrTouchCell(cell))
                return true;
        return false;
    }

    public boolean isOverlayOrTouchCell(Cell ctrlCell) {
        for (Cell cell : cells)
            for (int dx = -1; dx < 2; dx++)
                for (int dy = -1; dy < 2; dy++)
                    if (ctrlCell.getX() == cell.getX() + dx &&
                            ctrlCell.getY() == cell.getY() + dy)
                        return true;
        return false;
    }

    public boolean checkHit(int x, int y) {
        for (Cell cell : cells)
            if (cell.checkHit(x, y))
                return true;
        return false;
    }

    public boolean isAlive() {
        for (Cell cell : cells)
            if (cell.isAlive())
                return true;
        return false;
    }

    public void paint(Graphics g, int cellSize, boolean hide) {
        for (Cell cell : cells)
            cell.paint(g, cellSize, hide);
    }
}
