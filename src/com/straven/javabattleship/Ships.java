package com.straven.javabattleship;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

public class Ships {
    private final int cellSize;
    private final ArrayList<Ship> shipArrayList = new ArrayList<>(); // array for ship
    private final boolean hide;

    Ships(int fieldSize, int cellSize, boolean hide) {
        Random random = new Random();
        // pattern for ships
        int[] pattern = {4, 3, 3, 2, 2, 2, 1, 1, 1, 1};
        for (int j : pattern) {
            Ship ship;
            do {
                int x = random.nextInt(fieldSize);
                int y = random.nextInt(fieldSize);
                int position = random.nextInt(2);
                ship = new Ship(x, y, j, position);
            } while (ship.isOutOfField(fieldSize - 1) || isOverlayOrTouch(ship));
            shipArrayList.add(ship);
        }
        this.cellSize = cellSize;
        this.hide = hide;
    }

    public boolean isOverlayOrTouch(Ship ctrlShip) {
        for (Ship ship : shipArrayList) if (ship.isOverlayOrTouch(ctrlShip)) return true;
        return false;
    }

    public boolean checkHit(int x, int y) {
        for (Ship ship : shipArrayList) if (ship.checkHit(x, y)) return true;
        return false;
    }

    public boolean checkSurvivors() {
        for (Ship ship : shipArrayList) if (ship.isAlive()) return false;
        return true;
    }

    public void paint(Graphics g) {
        for (Ship ship : shipArrayList) ship.paint(g, cellSize, hide);
    }
}
