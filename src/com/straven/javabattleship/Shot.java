package com.straven.javabattleship;

import java.awt.Color;
import java.awt.Graphics;

public class Shot {
    private final int x;
    private final int y;
    private final boolean isShot;

    Shot(int x, int y, boolean isShot) {
        this.x = x;
        this.y = y;
        this.isShot = isShot;
    }

    public int getX() { return x; }
    public int getY() { return y; }
    public boolean isShot() { return isShot; }

    public void paint(Graphics g, int cellSize) {
        g.setColor(Color.gray);
        if (isShot)
            g.fillRect(x*cellSize + cellSize/2 - 3, y*cellSize + cellSize/2 - 3, 8, 8);
        else g.drawRect(x*cellSize + cellSize/2 - 3, y*cellSize + cellSize/2 - 3, 8, 8);
    }
}
