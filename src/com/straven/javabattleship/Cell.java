package com.straven.javabattleship;

import java.awt.Color;
import java.awt.Graphics;

public class Cell {
    private static final Color RED = Color.red;
    private final int x;
    private final int y;
    private Color color;

    Cell(int x, int y) {
        this.x = x;
        this.y = y;
        color = Color.gray;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    public boolean checkHit(int x, int y) {
        if (this.x == x && this.y == y) {
            color = RED; // change color if hit
            return true;
        }
        return false;
    }

    public boolean isAlive() {
        return color != RED; // judged by color
    }

    public void paint(Graphics g, int cellSize, boolean hide) {
        if (!hide || (color == RED)) {
            g.setColor(color);
            g.fill3DRect(x*cellSize + 1, y*cellSize + 1, cellSize - 2, cellSize - 2, true);
        }
    }
}
