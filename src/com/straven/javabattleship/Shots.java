package com.straven.javabattleship;

import java.awt.Graphics;
import java.util.ArrayList;

public class Shots {
    private final int cellSize;
    private final ArrayList<Shot> shotArrayList;

    Shots(int cellSize) {
        this.cellSize = cellSize;
        shotArrayList = new ArrayList<>();
    }

    public void add(int x, int y, boolean shot) {
        shotArrayList.add(new Shot(x, y, shot));
    }

    public boolean hitSamePlace(int x, int y) {
        for (Shot shot : shotArrayList)
            if (shot.getX() == x && shot.getY() == y && shot.isShot())
                return true;
        return false;
    }

    public Shot getLabel(int x, int y) {
        for (Shot label : shotArrayList)
            if (label.getX() == x && label.getY() == y && (!label.isShot()))
                return label;
        return null;
    }

    public void removeLabel(Shot label) {
        shotArrayList.remove(label);
    }

    public void paint(Graphics g) {
        for (Shot shot : shotArrayList) shot.paint(g, cellSize);
    }
}
